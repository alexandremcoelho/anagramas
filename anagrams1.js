const button = document.getElementById("findButton")
const button__reset = document.getElementById("reset")
let anagramas = []
let resultado = document.createElement("div")
resultado.id = "texto"

function getAnagramsOf (typedText) {
    let output = []
    texto = alphabetize(typedText)
    for(i=0; i<palavras.length; i++){
        if(alphabetize(palavras[i]) === texto){
        output = output + palavras[i] + " "
        }
    }
    return output
}


button.onclick = () => {
    let typedText = document.getElementById("input").value;
    let anagramas = document.createTextNode(getAnagramsOf(typedText))
    resultado.appendChild(anagramas)
    document.body.appendChild(resultado)
}

button__reset.onclick = () => {
    document.getElementById("texto").innerHTML = ""
}
    
function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
 }